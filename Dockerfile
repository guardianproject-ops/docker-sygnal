ARG SYGNAL_TAG
FROM docker.io/matrixdotorg/sygnal:$SYGNAL_TAG
RUN pip install logfmter
ENTRYPOINT ["python", "-m", "sygnal.sygnal"]
